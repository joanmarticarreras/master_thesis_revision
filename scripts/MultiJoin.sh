#!/bin/sh

# multijoin - join multiple files
#http://stackoverflow.com/questions/10726471/join-multiple-files

join_rec() {
    if [ $# -eq 1 ]; then
        join -t $'\t' - "$1"
    else
        f=$1; shift
        join -t $'\t' - "$f" | join_rec "$@"
    fi
}

if [ $# -le 2 ]; then
    join -t $'\t' - "$@"
else
    f1=$1; f2=$2; shift 2
    join -t $'\t' "$f1" "$f2" | join_rec "$@"
fi

