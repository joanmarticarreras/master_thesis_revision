# Scripts

This README.md contains a brief comment to each of the scripts used for this article:

## Metatranscriptomic read processing:

1) `Trimming.qsub`: Quality filters and clip the adaptars from the metatranscriptomic reads.

2) `rRNAIndex.qsub`: Indexes rRNA databases SILVA and Rfam within SortMeRNA program.

3) `rRNAFilter.qsub`: Filters rRNA and nrRNA fraction from metatranscriptomics reads.

4) `deinterleave_fastq.sh` and `DeinterleaveFastq.sh`: Utility scripts from managing read formats.


## DOM-RGC processing

1) `DOM_RGC.qsub`: Helps translating and indexing the DOM-RGC.

## MerA and MerB multiple-specific HMM modeling

1) `bHMM.qsub`: First step of the method, clusters sequences using 2 databases and creates the HMM database.

2) `fHMM.qsub`: Search step, models against the query dataset, plus the verification step against KEGG orthology database.

## Calculate abundance and expression counts

1) `MGAbundance.qsub`: Calculates abundance counts of metagenomic reads against the DOM-RGC, independently for each sample.

2) `MTExpression.qsub`: Calculates expression counts of metatranscriptomic reads against the DOM-RGC, independently for each sample.

3) `MultiJoin.sh`: Script used to paste together the counts for each sample.
