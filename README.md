# Supplementary Material to Biogeography of the present and expressed genes related to merAB methylmercury degradation retrieved from global deep ocean metagenomes and metatranscriptomes

Here is provided the scripts and plots used to obtain the results for the article  `Martí-Carreras, et al. Frontiers in Microbiology 2017`.

It is divided in 2 sections, scripts and plots. In `scripts/` there are containd all essential scripts for re-conducting the analysis, provided
there is access to the data. Another `READE.md` file is provided giving brief information for each script. The other folder, `plots/` there is all the information necessary to reproduce the graphic support in the article, as well as, all the metadata tables and count tables.

For any doubt, do not hestitate to contatct the corresponding author `joan.marti.carreras@gmail.com`.




PS: This project was created with the only purpose of sharing information with the revisors of my master thesis in bioinformatics. All intelectual
property linked to this original data belong to the authors of `Martí-Carreras, et al. Frontiers in Microbiology 2017`, Joan Martí-Carreras, Pablo Sánchez and Silvia G. Acinas.
